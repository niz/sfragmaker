# Source Fragmaker (sfragmaker)

[![License MIT](https://img.shields.io/badge/License-MIT-success.svg)](https://codeberg.org/niz/sfragmaker/src/branch/main/LICENSE)
[![Python 3.11](https://img.shields.io/badge/Python-3.11-blue.svg)](https://docs.python.org/3.11/)
[![Stability alpha](https://img.shields.io/badge/Stability-alpha-f4d03f.svg)]()

A CLI tool to help streamline the recording of video clips from [Source Engine demos](https://developer.valvesoftware.com/wiki/Demo_Recording_Tools).

<!-- vim-markdown-toc GFM -->

* [Description](#description)
  * [Reasoning](#reasoning)
  * [Features](#features)
    * [Current Features](#current-features)
    * [How it Works](#how-it-works)
  * [Requirements](#requirements)
* [Usage](#usage)
  * [Setting Up](#setting-up)
    * [Running with python3.11](#running-with-python311)
  * [Example](#example)
* [Configuration](#configuration)
* [Switching Profiles](#switching-profiles)
  * [Defining game settings](#defining-game-settings)
    * [Ultra](#ultra)
    * [Medium-low](#medium-low)
  * [Creating profiles](#creating-profiles)
  * [Loading a profile](#loading-a-profile)
* [FAQ](#faq)
  * [Why?](#why)
  * [Aren't there any similar (better) solutions?](#arent-there-any-similar-better-solutions)
  * [Don't you have something better to do than working on this?](#dont-you-have-something-better-to-do-than-working-on-this)
  * [Aren't you too late for the party?](#arent-you-too-late-for-the-party)
  * [Whats up with this name?](#whats-up-with-this-name)

<!-- vim-markdown-toc -->

## Description

This is a simple command line application with useful commands to create videos from Source Engine Demos. I'm currently developing and using this to make clips from [Team Fortress 2]( https://www.teamfortress.com/) demos on **Linux**. Although I plan to test this tool with different games and systems in the future, I cannot assert its performance with different parameters at the moment.

### Reasoning

The rendering of demo recordings as video clips, in the [original way](https://developer.valvesoftware.com/wiki/Demo_Video_Creation), is quite cumbersome:

- One must manually calculate the start and end ticks for given clip;
- Then, one must to type into the game console the commands to go to start tick and to stop playback at end tick;
- One must start and end the record manually;
- And when it's all recorded, one must use a different program to render the audio and frame files into a video, since the game does not have this functionality.

Sfragmaker does all of this, but with some scripting to streamline the process.

### Features

sfragmaker can help you to get information from your [.dem files](https://developer.valvesoftware.com/wiki/DEM_Format), and streamline the playback and recording of clips, as well as their rendering into videos. It can also make it easier to switch between different profiles of game settings.

#### Current Features

- [x] Probing of demo files by parsing the binaries from file's header.
- [x] Reading and writing of extra .json info file for demo (with content hashing to ensure the match).
- [x] Setting up of demo files to be played in game.
- [x] Setting up of clips to be recorded.
- [x] Saving and loading of clips from demo's info file.
- [x] Exporting of recorded demo files to different location outside game directory.
- [x] Exporting of recorded clips (frames and audio files) to different location outside game directory.
- [x] Switching of game settings between different profiles.
- [x] Rendering of clip frames into video files.
- [ ] Automatic brewing of coffee while rendering is being done.

#### How it Works

Actions and commands can be automated in a Source Engine game by the execution of [.cfg files](https://developer.valvesoftware.com/wiki/CFG), such as the ``autoexec.cfg`` file (where most of your cool configs are!)

Sfragmaker will generate custom .cfg files with the required commands to:

- play a demo, with ``playdemo <demo_name>``;
- play a clip, with ``demo_setendtick <end_tick>`` and ``demo_gototick <start_tick>``;
- start recording, with ``startmovie <clip_name>``.

It will generate three files that can be directly executed in game with ``exec <file_name>`` or through a bind set with ``bind <key> exec <file_name>``.

That's, pretty much the gist of it, to streamline some tedious tasks. Of course the .cfg files can be tweaked to fit anyone's needs (like changing render settings before playing a demo).

### Requirements

This is a hobby project that I am mostly using to test out some concepts and play around with the newer Python releases.

- Requires **Python 3.11** for the [toml parsing](https://docs.python.org/3.11/library/tomllib.html).

- Requires [ffmpeg](https://ffmpeg.org/) for video rendering and encoding.

## Usage

### Setting Up

Clone the repository, or download and extract it somewhere. Inside the directory run the command:

```
pip install .
```

Print command usage with:

```
sfg --help
```

#### Running with python3.11

Most systems are not running 3.11 as their default python3 version, and to make pip work with it might be a hassle.

Sfragmaker doesn't have any  a lot dependencies outside of the core Python libraries, so we can get away with just modifying ``~/.local/bin/sfg`` to require python3.11 instead of python3. You might need to add the project's root directory to your ``PYTHONPATH`` environmental variable.

### Example

Let's run through a scenario where I record a clip from a demo file in TF2.

Loading my demo file:

```
$ sfg load demo pl_goldrush.dem
```

- If a ``pl_goldrush.json`` info file could not be found, a new one would be created.

Checking the current loaded demo:

```
$ sfg current info
```

Output:

```
[Current Demo] /home/niz/demos/pl_goldrush.dem
--------------------------------------------------------------------------------
Client: Niz | Map: pl_goldrush | Server: 169.254.41.159:13608
--------------------------------------------------------------------------------
Playback Time: 0:42:45
--------------------------------------------------------------------------------
Frames: 78690 | Ticks: 171017
--------------------------------------------------------------------------------
Clips: 0 | Events: 2 | Tags: 0
--------------------------------------------------------------------------------

No clip currently loaded.
```

Events are created during gameplay, automatically or by using the ``ds_mark`` command to set a bookmark. Looks like we have 2 of them.

Listing current events:

```
$ sfg current list events
```

Output:

```
[Current Demo] /home/niz/demos/pl_goldrush.dem
--------------------------------------------------------------------------------
Events:

 [1] 27236 Bookmark General
 [2] 27435 Killstreak 4

--------------------------------------------------------------------------------
```

Event ``1`` is a bookmark and looks interesting, I need to replay the demo in game to check it out.

The demo is already set in the ``sfg_playdemo.cfg`` file. It can be executed in game with ``exec sfg_playdemo`` (I have it bound to my ``F9`` key).

With the game open, I can start demo replay (pressing ``F9`` on my case):

```
TF2 Console: exec sfg_playdemo
```

I can go to my bookmarked tick using the ``demoui`` menu or executing the game command ``demo_gototick``. I usually set bookmarks _after_ something of interest has happen, so I would like to go a couple of ticks before it.

Going to tick:

```
TF2 Console: demo_gototick 26200
```

Looks like my bookmark is for a cool triple kill with a reflected rocket as pyro! I have chosen the timestamps and I would like to make a clip.

Creating a new clip:

```
sfg current add clip triple_reflect_kill
```

It opens a dialog to create a new clip:

```
[Current Demo]: /home/niz/demos/pl_goldrush.dem
--------------------------------------------------------------------------------

 New clip:

 Name: triple_reflect_kill
 Description: Triple kill with a reflected rocket
 Start: 6:30
 End:
 Length: 20
 Tags: pyro reflect


[Current Demo] /home/niz/demos/pl_goldrush.dem
--------------------------------------------------------------------------------
Clips:

 [1] triple_reflect_kill (0:06:30 - 0:06:50)
 Description: Triple kill with a reflected rocket
 --------
 Tags: pyro, reflect


--------------------------------------------------------------------------------
```

- The clip is now saved to ``pl_goldrush.json`` and I can load it every time this demo is loaded.

I would like to record this clip, but before that I want to switch to my _recording_ profile, with prettier game settings. Unfortunately, it requires a game restart, so I should close TF2.

Loading the _recording_ profile:

```
$ sfg load profile recording
```

- Launching my game again, the new settings are applied.

I can now load my clip (demo is already loaded). Loading clip:

```
$ sfg load clip 1
```

Checking current info:

```
$ sfg current info
```

Output:

```
[Current Demo] /home/niz/projects/sfragmaker/pl_goldrush.dem
--------------------------------------------------------------------------------
Client: Niz | Map: pl_goldrush | Server: 169.254.41.159:13608
--------------------------------------------------------------------------------
Playback Time: 0:42:45
--------------------------------------------------------------------------------
Frames: 78690 | Ticks: 171017
--------------------------------------------------------------------------------
Clips: 1 | Events: 2 | Tags: 0
--------------------------------------------------------------------------------

[Current Clip]
--------------------------------------------------------------------------------
triple_reflect_kill
--------------------------------------------------------------------------------
Start: 0:06:30
End: 0:06:50
--------------------------------------------------------------------------------
Duration: 0:00:20
--------------------------------------------------------------------------------
```

Similarly to demo playback, the setting up of clips is done by the ``sfg_setclip.cfg`` file. It can be executed in game with ``exec sfg_setclip`` (I have it bound to my ``F8`` key).


Setting up clip (pressing ``F8`` on my case):

```
TF2 Console: exec sfg_setclip
```

This will move the playback to the start tick of the clip, and tell the game to stop it at its end tick.

Now I just need to hit _record_, and for that the ``sfg_record.cfg`` file is used. It can be executed in game with ``exec sfg_record`` (I have it bound to my ``F7`` key).

Start recording (pressing ``F7`` on my case):

```
TF2 Console: exec sfg_record
```

The recording starts. It might look somewhat glitchy for someone that has never done this before, but it's just how it goes (better turn off your sound).

When the clip is over, the demo playback is closed. I can stop the recording with ``stopmovie`` (I have it boud to my ``F6`` key).

Stopping movie recording (pressing ``F6`` on my case):

```
TF2 Console: stopmovie
```

The recording is done, I can now export my audio and frame files to a more suitable location.

Exporting recording:

```
$ sfg export recording -d ~/videos/frames/triple_reflect_kill
```

- The recording files where moved to the new directory.

I can now render my video, but the default rendering settings are meant for quality and can be very slow. I will use the ``-s`` option to render in speed mode, I will also use the ``-a`` option to tell ffmpeg to add the audio file.

Rendering video in speed mode:

```
$ sfg render -sa ~/videos/frames/triple_reflect_kill -o ~/videos/triple_reflect_kill.mp4
```

I can now check my clip and decide if everything is alright with recording, or if I'll have to tweak something and to it again (usually it goes alright).

I'm happy with my clip, and I would like to render if with the best quality. It might take several minutes.

Rendering video with final quality (very slow):

```
$ sfg render -a ~/videos/frames/triple_reflect_kill -o ~/videos/triple_reflect_kill_final.mp4
```

I'm done recording and my game is already closed. I'll switch back to my _gaming_ settings so I don't forget it next time I want to play again.

```
$ sfg load profile gaming
```

It might also be good practice to remove all the ``.cfg`` files by unloading my demo.

Unloading demo:

```
$ sfg unload
```

- All ``.cfg`` files created by sfragmaker have been removed.

You can check out the final video [here](https://www.youtube.com/watch?v=jjBtEK_7H9U).


## Configuration

Sfragmaker will look for user defined configurations in ``$XDG_CONFIG_HOME/sfragmaker`` and then ``$HOME/.config/sfragmaker``. The default files can be copied from ``./sfg/resources/config`` inside sfragmaker's directory, and must follow their original hierarchy.

```
~/.config/sfragmaker
├── cfg
│   ├── playdemo.cfg
│   ├── record.cfg
│   └── setclip.cfg
├── profiles.toml
└── sfragmaker.toml
```

The main configuration file, ``sfragmaker.toml`` defines default paths and variables.

The .cfg files are used as template for the ones created inside the game's ``./cfg`` directory. They can be customized to add or remove game settings and commands to suit the user's needs.

``profiles.toml`` is where all the game settings profiles are defined.

## Switching Profiles

One of the upsides of creating clips from demo files, as opposite of screen recording, is that we can use different settings for gaming and recording. One might desire do play with a more fps-oriented config (lower details, no gibs, etc), and still render their videos with the prettiest settings.

I've been calling these different groups of settings "Profiles".

Let's run through an example of setting up these profiles.

### Defining game settings

Before setting up our profiles, we need to have our game settings defined, I highly recommend [mastercomfig](https://mastercomfig.com/), it makes this sort of things so easy!

I'll choose the **ultra** preset for **recording**, and a **medium-low** for **gaming** in my ancient laptop. I'll follow their instructions to download and setup those presets.

Mastercomfig makes it very painless to switch between its own presets, by changing the ``pre_comfig.cfg`` file. I also want to enable anti-aliasing on my ultra preset, so I will have to edit the ``modules.cfg`` file as well.

#### Ultra

**pre_comfig.cfg**
```
preset=ultra
```

**modules.cfg**
```
sound=very_high
anti_aliasing=msaa_8x
motion_blur=high
```

#### Medium-low

**pre_comfig.cfg**
```
preset=medium-low
```

**modules.cfg**
```
sound=very_high
```

### Creating profiles

We now know that the main difference between the profiles **recording**  and **gaming** are these two files that need to be swapped, so we can start editing our ``profiles.toml``:

**profiles.toml**
```
[gaming]
files = [
['~/my_safe_dir/medium-low/pre_comfig.cfg', '{GAME_DIR}/cfg/overrides/pre_comfig.cfg'],
['~/my_safe_dir/medium-low/modules.cfg', '{GAME_DIR}/cfg/overrides/modules.cfg'],
]

[recording]
files = [
['~/my_safe_dir/ultra/pre_comfig.cfg', '{GAME_DIR}/cfg/overrides/pre_comfig.cfg'],
['~/my_safe_dir/ultra/modules.cfg', '{GAME_DIR}/cfg/overrides/modules.cfg'],
]
```

Note that my original files are safely stored in ``~/my_safe_dir``, and they will be copied to the location defined by the second part of the pair. All files must be defined with this ``[source, destination]`` format.

### Loading a profile

We can now load our recording profile with:

```
$ sfg load profile record
```

- Most preset changes will probably require a game restart to take effect.

Note that the default behaviour of this action is not to replace existing files with the profile-defined ones, but to **remove every file related to other profiles** and then copying over the ones needed. This is why you must keep your original files in a safe location, but also the reason you can define as many profiles as you need.

## FAQ

### Why?

Why not? Okay, I get it.

### Aren't there any similar (better) solutions?

Yes! And a lot of people way more knowledgeable than I am on this subject. But that has never stopped me before!

In all fairness I've tried to use the [lawena recording tool](https://lawena.github.io/), but I didn't quite made it work on my Linux machine. Well... I didn't even try too hard, to be honest. The project has been archived and the last update was released a long time ago. I was in the process of learning about what exactly the program does, so I could maybe try and fiddle with the source code to do what I wanted to do, when I realized most of these tools only automate some tasks and provide a nice GUI wrapper for native functions of the Source game.

I was already doing it "by hand" with Team Fortress 2 and some bash scripts to make my life easier whilst rendering the clips with ffmpeg, so I just decided it would be a cool learning experience to make my own command line application for it. And, as I always reply to everyone that asks me why I like the command line so much, if you can run it on the terminal, you can put it on a file and automate the hell out of it!

### Don't you have something better to do than working on this?

Don't you have something better to do than reading this?

### Aren't you too late for the party?

Yeah... I was coding until late last night, and I only really meant to take a short nap... but I lost the time and slept through most of it. But there is still a party right? I can see some people around here... I think a lot of them are bots... but still! If there is some fun yet to be had, by Gaben, I'm having it!

### Whats up with this name?

Come on! The joke was too obvious not to be made.

It's just a play on [Valve's Source Filmmaker](https://store.steampowered.com/app/1840/Source_Filmmaker/), since the only movies I want to make are those sick sick frag movies.
