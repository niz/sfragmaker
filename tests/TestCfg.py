import io
import unittest
from pathlib import Path

from sfg.core import cfg

cfg_str ="""// This represents an actual cfg file.
// It doesn't have any relevant information but keeps the format.
// -------------------------------------------------------------
// original = "{original}"
// hash = "{hash}"
"""

"""Data encoded by cfg_str."""
cfg_data = {
    'original': '{original}',
    'hash': '{hash}'
}

"""Section of default configurations relevant to these tests."""
default_config = {
    'game_dir': '~/.local/share/Steam/steamapps/common/Team Fortress 2/tf',
    'cfg_prefix': 'sfg_',
}

class TestCfg(unittest.TestCase):

    def test_read_file(self):
        file = io.StringIO(cfg_str)
        file_data = cfg.read_file(file, lines=2, skip=3)
        self.assertEqual(cfg_data, file_data)

    def test_format_file(self):
        resource_file = io.StringIO('My text file is {STATUS}.')
        subs = {'STATUS': 'formatted'}
        text = cfg.format_file(resource_file, subs)
        self.assertEqual(text, 'My text file is formatted.')

    def test_write_and_read_file(self):
        res_file = io.StringIO(cfg_str)
        subs = {'original': 'My demo', 'hash': 'demo_hash'}

        # "Writing" to file.
        file = io.StringIO(cfg.format_file(res_file, subs))
        data = cfg.read_file(file, lines=2, skip=3)
        self.assertEqual(subs, data)

    def test_get_file_path_playdemo(self):
        default_path = Path(
            default_config['game_dir'],
            'cfg',
            f"{default_config['cfg_prefix']}playdemo.cfg")
        default_path = default_path.expanduser()

        path = cfg.get_file_path('playdemo', force_default=True)
        self.assertEqual(default_path, path)

    def test_get_file_path_record(self):
        default_path = Path(
            default_config['game_dir'],
            'cfg',
            f"{default_config['cfg_prefix']}record.cfg")
        default_path = default_path.expanduser()

        path = cfg.get_file_path('record', force_default=True)
        self.assertEqual(default_path, path)

    def test_get_file_path_setclip(self):
        default_path = Path(
            default_config['game_dir'],
            'cfg',
            f"{default_config['cfg_prefix']}setclip.cfg")
        default_path = default_path.expanduser()

        path = cfg.get_file_path('setclip', force_default=True)
        self.assertEqual(default_path, path)

    def test_get_file_path_unexpected(self):
        with self.assertRaises(FileNotFoundError):
            cfg.get_file_path('popcorn')

    # Testing for default resource files.
    # --------------------------------

    def test_get_resources_dir_default(self):
        # Default cfg directory: ../sfg/resources/config/cfg/
        main_dir = Path(__file__).resolve().parents[1]
        default_dir = main_dir.joinpath('sfg', 'resources', 'config', 'cfg')
        default_dir = default_dir.resolve()

        path = cfg.get_resources_dir(force_default=True)
        self.assertEqual(default_dir, path)

    def test_get_resource_file_path_playdemo(self):
        # Default cfg directory: ../sfg/resources/config/cfg/
        main_dir = Path(__file__).resolve().parents[1]
        default_path = main_dir.joinpath(
            'sfg',
            'resources',
            'config',
            'cfg',
            'playdemo.cfg')

        default_path = default_path.resolve()

        path = cfg.get_resource_file_path('playdemo', force_default=True)
        self.assertEqual(default_path, path)

    def test_get_resource_file_path_record(self):
        # Default cfg directory: ../sfg/resources/config/cfg/
        main_dir = Path(__file__).resolve().parents[1]
        default_path = main_dir.joinpath(
            'sfg',
            'resources',
            'config',
            'cfg',
            'record.cfg')

        default_path = default_path.resolve()

        path = cfg.get_resource_file_path('record', force_default=True)
        self.assertEqual(default_path, path)

    def test_get_resource_file_path_setclip(self):
        # Default cfg directory: ../sfg/resources/config/cfg/
        main_dir = Path(__file__).resolve().parents[1]
        default_path = main_dir.joinpath(
            'sfg',
            'resources',
            'config',
            'cfg',
            'setclip.cfg')

        default_path = default_path.resolve()

        path = cfg.get_resource_file_path('setclip', force_default=True)
        self.assertEqual(default_path, path)

    def test_get_resource_file_path_unexpected(self):
        with self.assertRaises(FileNotFoundError):
            path = cfg.get_resource_file_path(
                'popcorn',
                force_default=True)

if __name__ == '__main__':
    unittest.main()
