import unittest

from sfg.core import ticktime

class TestTickTime(unittest.TestCase):

    # Secs to Ticks
    def test_secs_to_ticks_default_tps(self):
        ticks = ticktime.secs_to_ticks(10)
        self.assertEqual(ticks, 667)

    def test_secs_to_ticks_custom_tps(self):
        ticks = ticktime.secs_to_ticks(10, 60)
        self.assertEqual(ticks, 600)

    def test_secs_to_ticks_unexpected_secs(self):
        with self.assertRaises(TypeError):
            ticktime.secs_to_ticks('popcorn')

    def test_secs_to_ticks_unexpected_tps(self):
        with self.assertRaises(TypeError):
            ticktime.secs_to_ticks(10, 'popcorn')

    # Ticks to Secs
    def test_ticks_to_secs_default_tps(self):
        secs = ticktime.ticks_to_secs(667)
        self.assertEqual(secs, 10)

    def test_ticks_to_secs_custom_tps(self):
        secs = ticktime.ticks_to_secs(600, 60)
        self.assertEqual(secs, 10)

    def test_ticks_to_secs_unexpected_secs(self):
        with self.assertRaises(TypeError):
            ticktime.ticks_to_secs('popcorn')

    def test_ticks_to_secs_unexpected_tps(self):
        with self.assertRaises(TypeError):
            ticktime.ticks_to_secs(10, 'popcorn')

    # Time to Secs
    def test_time_to_secs_short(self):
        secs = ticktime.time_to_secs('20')
        self.assertEqual(secs, 20)

    def test_time_to_secs_medium(self):
        secs = ticktime.time_to_secs('01:01')
        self.assertEqual(secs, 61)

    def test_time_to_secs_long(self):
        secs = ticktime.time_to_secs('01:01:01')
        self.assertEqual(secs, 3661)

    def test_time_to_secs_too_long(self):
        with self.assertRaises(ValueError):
            ticktime.time_to_secs('01:01:01:01')

    def test_time_to_secs_bad_format(self):
        with self.assertRaises(ValueError):
            ticktime.time_to_secs('popcorn:01:01')

    # Time to Ticks
    def test_time_to_ticks_short_default_tps(self):
        ticks = ticktime.time_to_ticks('10')
        self.assertEqual(ticks, 667)

    def test_time_to_ticks_medium_default_tps(self):
        ticks = ticktime.time_to_ticks('01:01')
        self.assertEqual(ticks, 4067)

    def test_time_to_ticks_long_default_tps(self):
        secs = ticktime.time_to_ticks('01:01:01')
        self.assertEqual(secs, 244067)

    def test_time_to_ticks_short_custom_tps(self):
        ticks = ticktime.time_to_ticks('10', 60)
        self.assertEqual(ticks, 600)

    def test_time_to_ticks_medium_custom_tps(self):
        ticks = ticktime.time_to_ticks('01:01', 60)
        self.assertEqual(ticks, 3660)

    def test_time_to_ticks_long_custom_tps(self):
        ticks = ticktime.time_to_ticks('01:01:01', 60)
        self.assertEqual(ticks, 219660)

    def test_time_to_ticks_unexpected_tps(self):
        with self.assertRaises(TypeError):
            ticktime.time_to_ticks('01:01:01', 'popcorn')

    def test_time_to_secs_too_long(self):
        with self.assertRaises(ValueError):
            ticktime.time_to_ticks('01:01:01:01')

    def test_time_to_secs_bad_format(self):
        with self.assertRaises(ValueError):
            ticktime.time_to_ticks('popcorn:01:01')

    # Ticks to Time
    def test_ticks_to_time_default_tps(self):
        time = ticktime.ticks_to_time(667)
        self.assertEqual(time, '0:00:10')

    def test_ticks_to_time_custom_tps(self):
        time = ticktime.ticks_to_time(600, 60)
        self.assertEqual(time, '0:00:10')

    def test_ticks_to_time_unexpected_value(self):
        with self.assertRaises(TypeError):
            ticktime.ticks_to_time('popcorn')

    # Secs to Time
    def test_secs_to_time(self):
        time = ticktime.secs_to_time(60)
        self.assertEqual(time, '0:01:00')

    def test_secs_to_time_unexpected_value(self):
        with self.assertRaises(TypeError):
            ticktime.secs_to_time('popcorn')

if __name__ == '__main__':
    unittest.main()
