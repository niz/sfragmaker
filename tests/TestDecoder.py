import io
import unittest

from sfg.core import decoder

"""Bytes copied from a real demo file."""
file_bytes = b"".join([
    b'HL2DEMO\x00',
    b'\x03\x00\x00\x00',
    b'\x18\x00\x00\x00',
    b'255.255.255.255:25555',
    b'\x00'*239,
    b'Niz',
    b'\x00'*257,
    b'ctf_2fort',
    b'\x00'*251,
    b'tf',
    b'\x00'*258,
    b'p}\xadC',
    b'\\Z\x00\x00',
    b'\x11Y\x00\x00',
    b'\x85\xa1\x04\x00',
])

"""Data represented by file_bites."""
file_data =  {
    'header': 'HL2DEMO',
    'demo_protocol': 3,
    'network_protocol': 24,
    'server': '255.255.255.255:25555',
    'client': 'Niz',
    'map': 'ctf_2fort',
    'game_directory': 'tf',
    'playback_time': 346.97998046875,
    'ticks': 23132,
    'frames': 22801,
    'sign_on_length': 303493,
}

class TestDecoder(unittest.TestCase):

    def test_read_demo(self):

      fake_file = io.BytesIO(file_bytes)
      data = decoder.decode(fake_file)

      for key, value in file_data.items():
          self.assertEqual(data[key], value)

    def test_read_not_a_demo(self):

        bad_bytes = file_bytes[1:]
        fake_file = io.BytesIO(bad_bytes)

        with self.assertRaises(TypeError):
            decoder.decode(fake_file)

    def test_read_truncaded_demo(self):

        bad_bytes = file_bytes[0:512]
        fake_file = io.BytesIO(bad_bytes)

        with self.assertRaises(ValueError):
            decoder.decode(fake_file)

if __name__ == '__main__':
    unittest.main()
