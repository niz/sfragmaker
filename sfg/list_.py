#!/usr/bin/env python3.11
# Description: Lists clips, events and tags for currently loaded
# demo.
# Requires: Python 3.11
# --------------------------------------------------------------

import argparse

from . import info
from . import reporter

def report_list(args):
    """Report a list of items."""
    demo = info.get_demo_or_fail()
    reporter.report(
        demo.report_list(args.option, prepend='[Current Demo] '))

def main(args=None, prog='list'):
    """Main entry point for this module."""

    # Command: current.
    # --------------------------------
    parser = argparse.ArgumentParser(
        prog=prog,
        description=('Lists clips, events and tags for currently'
                     ' loaded demo.'))

    parser.add_argument(
        'option',
        type=str,
        choices=['clips', 'events', 'tags'],
        help='Information to report.')

    # Parse arguments
    # --------------------------------
    args = parser.parse_args(args)

    # When called without arguments.
    if not any(vars(args).values()):
        parser.print_usage()
        exit()

    report_list(args)

if __name__ == '__main__':
        main()

