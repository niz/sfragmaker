#!/usr/bin/env python3.11
# Description: Subcommand to work with currently loaded demo.
# Requires: Python 3.11
# --------------------------------------------------------------

import argparse

from . import add
from . import edit
from . import info
from . import list_
from . import remove

def main(args=None, prog='current'):
    """Main entry point for this module."""

    # Defined subcommands.
    subcommands = {
        'add': add.main,
        'edit': edit.main,
        'info': info.main,
        'list': list_.main,
        'remove': remove.main,
    }

    # Command: currentj
    # --------------------------------
    parser = argparse.ArgumentParser(
        prog=prog,
        description=('Works with currently loaded demo'))

    parser.add_argument(
        'subcommand',
        type=str,
        choices=subcommands.keys(),
        help='Subcommand to be called.')

    # When called without arguments, print usage instead.
    if len(args) < 1:
        parser.print_usage()
        exit()

    # Only consider first argument as valid.
    first = [args[0]]
    sub_name = parser.parse_args(first).subcommand

    # Use anything else as subcommand argument.
    sub_args = args[1:]

    # Call subcommand main function with remaining arguments.
    subcommands[sub_name](sub_args, f"{prog} {sub_name}")

if __name__ == '__main__':
        main()

