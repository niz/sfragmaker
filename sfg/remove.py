#!/usr/bin/env python3.11
# Description: Removes clips, events or tags from current demo.
# Requires: Python 3.11
# --------------------------------------------------------------

import argparse

from . import info
from . import list_
from . import reporter

def remove_clip(args):
    """Remove clip from current demo."""
    demo = info.get_demo_or_fail()
    demo.remove_clip(index=args.index)

    list_.main(args=['clips'])

def remove_event(args):
    """Remove event from current demo."""
    demo = info.get_demo_or_fail()
    demo.remove_event(args.index)

    list_.main(args=['events'])

def remove_tag(args):
    """Remove tags from current demo."""
    demo = info.get_demo_or_fail()
    demo.remove_tag(args.index)

    list_.main(args=['tags'])

def main(args=None, prog='remove'):
    """Main module's entry point."""

    options = {
        'clip': remove_clip,
        'event': remove_event,
        'tag': remove_tag,
    }

    # Root command: add.
    # --------------------------------
    parser = argparse.ArgumentParser(
        prog=prog,
        description='Removes clips, events or tags from current demo file.')

    parser.add_argument(
        'item',
        type=str,
        choices=options.keys(),
        help='Type of item to be removed.')

    parser.add_argument(
        'index',
        type=int,
        help='Index of item to be removed.')

    # Parsing arguments.
    # --------------------------------
    args = parser.parse_args(args)

    # When called without arguments.
    if not any(vars(args).values()):
        parser.print_usage()
        exit()

    # Execute sub command functions
    options[args.item](args)

if __name__ == '__main__':
    main()
