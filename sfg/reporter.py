#!/usr/bin/env python3.11
# Description: Formats and reports messages for sfragmaker.
# Requires: Python 3.11
# --------------------------------------------------------------

from sys import exit

def report(message):
    """Prints formatted message."""
    print(format(message))

def format(message):
    """Formats messages to be printed."""

    formats = {
        "SEPARATOR": f"\n{'-'*80}\n"
    }

    return message.format(**formats)

def exit_with(key, **kwargs):
    """Exits with defined message."""

    messages = {
        'missing_current_demo': 'No demo currently loaded.',
        'missing_current_clip': 'No clip currently loaded.',
        'missing_index': 'Could not find {item} at index {index}',
    }

    report(messages[key].format(**kwargs))
    exit()
