#!/usr/bin/env python3.11
# Description: Renders clip recordings for sfragmaker.
# Requires: Python 3.11, ffmpeg
# --------------------------------------------------------------

import argparse

from . import reporter

from .core import renderer
from .core import config as cf

def render(args):
    """Renders clip recordings into videos."""

    renderer.render(
        args.input,
        output=args.output,
        preset=args.preset,
        crf=args.crf,
        frame_rate=args.frame_rate,
        audio=args.audio,
        override=args.yes,
        speed_mode=args.speed_mode)

def main(args=None, prog='render'):
    """Command's entry point."""

    parser = argparse.ArgumentParser(
        prog=prog,
        description='Renders clip recordings into videos.')

    parser.add_argument(
        'input',
        help='Path to directory where frame files are located.')

    parser.add_argument(
        '-o',
        '--output',
        help='Output file.')

    parser.add_argument(
        '-a',
        '--audio',
        action='store_true',
        help='Include audio source.')

    parser.add_argument(
        '-p',
        '--preset',
        help='ffmpeg preset.')

    parser.add_argument(
        '-c',
        '--crf',
        help='Constant rate factor CRF.')

    parser.add_argument(
        '-f',
        '--frame_rate',
        help='Frame rate (frames per second).')

    parser.add_argument(
        '-l',
        '--log',
        action='store_true',
        help='Save log file.')

    parser.add_argument(
        '-y',
        '--yes',
        action='store_true',
        help='Override existing output file.')

    parser.add_argument(
        '-s',
        '--speed_mode',
        action='store_true',
        help='Speed Mode. Will render at lower quality.')

    args = parser.parse_args(args)
    render(args)
