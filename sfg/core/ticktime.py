# Description: Functions to convert between ticks and time strings.
# Requires: Python 3.11
# --------------------------------------------------------------

from datetime import timedelta

"""Ticks Per Second may vary depending
on the server. This is the default value
for Team Fortress 2 official servers.
"""
TPS = 66.66666666

def secs_to_ticks(secs, tps=TPS):
    """Convert seconds to ticks."""
    return int(round(check_number(secs)*check_number(tps)))

def ticks_to_secs(ticks, tps=TPS):
    """Convert ticks to seconds."""
    return int(round(check_number(ticks)/check_number(tps)))

def time_to_ticks(time_str, tps=TPS):
    """Convert HH:mm:ss time string to ticks."""
    return secs_to_ticks(time_to_secs(time_str), tps)

def ticks_to_time(ticks, tps=TPS):
    """Convert ticks to HH:mm:ss time string."""
    return secs_to_time(ticks_to_secs(ticks, tps))

def secs_to_time(secs):
    """Convert seconds to HH:mm:ss time string."""
    return str(timedelta(seconds=secs))

def time_to_secs(time_str):
    """Convert HH:mm:ss time string to total seconds."""
    time = time_str.split(':')
    if len(time) <= 3:
        try:
            time.reverse()
            secs = 0
            for index, value in enumerate(time):
                secs += int(value) * (60**index)
            return secs
        except:
            pass

    raise ValueError(
        f"Badly formatted time string: {time_str}.")

def check_number(value):
    """Check if value is a number and return value."""
    try:
        int(value)
        return value
    except:
        pass

    raise TypeError(
        f"Unexpected parameter: {value} is not a number.")
