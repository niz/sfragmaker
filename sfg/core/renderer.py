#!/usr/bin/env python3.11
# Description: Renders clip recordings for sfragmaker.
# Requires: Python 3.11, ffmpeg
# --------------------------------------------------------------

import os
import subprocess
import shutil
from pathlib import Path

from . import config as cf

def render(
        input,
        output=None,
        preset=None,
        crf=None,
        frame_rate=None,
        audio=False,
        override=False,
        speed_mode=False):
    """Renders clip recordings into videos."""

    input_dir = Path(input)
    if not input_dir.is_dir():
        raise Exception(f"Invalid input {input}")

    frame_count = count_frames(input_dir)
    if not frame_count:
        raise Exception(
            f"Could not find any frame files inside: {input}")

    # TODO: Report
    print(f"Found {frame_count} fames.")

    audio_source = get_audio_source(input_dir)
    if audio and not audio_source:
        raise Exception(
            f"Could not find audio file inside {input}")

    parameters = get_default_parameters()

    if preset:
        parameters['PRESET'] = preset

    if crf:
        parameters['CRF'] = crf

    if frame_rate:
        parameters['FRAME_RATE'] = frame_rate

    if speed_mode:
        parameters['PRESET'] = 'ultrafast'
        parameters['CRF'] = 28

    parameters['OVERRIDE'] = '-y' if override else ''

    if output:
        parameters['OUTPUT_FILE'] = output
    else:
        parameters['OUTPUT_FILE'] = get_output(input_dir, parameters)

    parameters['AUDIO_SOURCE'] = audio_source
    parameters['VIDEO_SOURCE'] = input_dir.joinpath('*.tga')

    audio_file = input_dir.joinpath('audio.aac')
    parameters['AUDIO_FILE'] = audio_file

    video_file = input_dir.joinpath('video.mp4')
    parameters['VIDEO_FILE'] = video_file

    encode('video', parameters)

    if audio:
        encode('audio', parameters)
        encode('merge', parameters)

        # Removing temporary files.
        if video_file.is_file():
            os.remove(video_file)

        if audio_file.is_file():
            os.remove(audio_file)

    else:
        if output.is_file() and not override:
            reply = input(f"Replace existing file {output}? (y/n) ")
            if reply.lower() != 'y' and reply.lower() != 'yes':
                exit()

        shutil.move(video_file, output)

def encode(option, parameters):
    """Calls ffmpeg to encode frames into video."""

    options = {
        'video': ('ffmpeg -hide_banner -y -framerate {FRAME_RATE}'
                  ' -pattern_type glob -i {VIDEO_SOURCE} -c:v libx264'
                  ' -crf {CRF} -preset {PRESET} -an {VIDEO_FILE}'),

        'audio': ('ffmpeg -hide_banner -y -i {AUDIO_SOURCE} -ab 192k'
                  ' -c:a aac {AUDIO_FILE}'),

        'merge': ('ffmpeg -hide_banner {OVERRIDE} -i {VIDEO_FILE}'
                  ' -i {AUDIO_FILE} -c copy -map 0:v -map 1:a'
                  ' {OUTPUT_FILE}'),
    }

    command = list(filter(None, options[option].format(**parameters).split(' ')))
    subprocess.run(command)

def count_frames(input_dir):
    """Counts frames inside directory."""

    frame_count = 0
    for frame in input_dir.glob('*.tga'):
        frame_count += 1
    return frame_count

def get_output(input_dir, parameters):
    """Formats output from configurations."""

    config = cf.load()

    if 'CLIP_NAME' not in parameters.keys():
        parameters['CLIP_NAME'] = Path(input_dir).stem

    file_name = config.encoding.file_name.format(**parameters)
    file = f"{file_name}.{config.encoding.container}"
    return Path(config.export.videos_dir, file)

def get_default_parameters():
    """Gets default encoding parameters from configurations."""
    config = cf.load().encoding

    params = {}
    for key, value in vars(config).items():
        params[key.upper()] = value

    return params

def get_audio_source(input_dir):
    """Gets audio file inside directory."""
    return next(input_dir.glob("*.wav"), None)

