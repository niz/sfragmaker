# Description: Manages game settings profiles.
# Requires: Python 3.11
# --------------------------------------------------------------

import fileinput
import os
import shutil
import subprocess
import tomllib
from pathlib import Path

from . import config

"""Default file where profiles are defined."""
file_name = "profiles.toml"

def load(profile):
    """Loads new profile."""

    data = read_file()
    if not data:
        raise Exception('Could not load game settings profiles.')

    # Profile has to be a valid section in swaps file.
    if profile not in data.keys():
        raise Exception(f"Invalid profile: {profile}")

    # Autorun script before messing around with files.
    if 'autorun' in data[profile].keys():
        subprocess.run(resolve_path(data[profile]['autorun']))

    # Files from different profiles should be removed.
    to_remove = []
    for section, content in data.items():
        if section != profile:
            for file in content['files']:
                to_remove.append(resolve_path(file[1]))

    for file in to_remove:
        if file.is_file():
            os.remove(file)

    for file in data[profile]['files']:
        src = resolve_path(file[0])
        dst = resolve_path(file[1])

        shutil.copy(src, dst)

    launch_options = data[profile]['launch']
    replace_launch_options(launch_options)

def read_file():
    """Reads profiles toml file."""

    file = get_file()
    with open(file, 'rb') as f:
        data = tomllib.load(f)
        return data

def get_file():
    """Gets the path for profiles file."""

    config_home = config.get_config_home()

    file_path = config_home.joinpath(file_name)
    if not file_path.is_file():
        print(f"Could not locate {file_name} in configurations folder")
        exit()

    return file_path

def resolve_path(path):
    """Returns resolved Path objects from path strings."""

    path = expand_vars(path)
    return Path(path).expanduser().resolve()

def expand_vars(path):
    """Expands paths with allowed variables."""

    replacements = {
        '{GAME_DIR}': config.load().game.game_dir.resolve(),
    }

    for key, value in replacements.items():
        path = path.replace(key, str(value))

    return path

def replace_launch_options(new_options):
    """Replaces launch options in file.
    This function is very experimental and should
    be used only if proper backups were previously
    made."""

    def replace_str(old, new):
        """Helper function to replace old options with new."""
        quotes = 0
        start = ''

        for char in old:
            start += char
            if char == '"':
                quotes += 1
            if quotes == 3:
                break

        return f"{start}{new_options}\"\n"

    steam = config.load().steam
    if not steam.edit_file:
        return

    vdf_file = steam.localconfig

    with fileinput.input(files=(vdf_file), inplace=True) as f:

        inside_software = False
        inside_game = False

        for line in f:
            clean_line = line.strip()
            if clean_line.startswith('"Software"'):
                inside_software = True

            if inside_software:
                if clean_line.startswith(f"\"{steam.game_id}\""):
                    inside_game = True

            if inside_game:
                if clean_line.startswith('"LaunchOptions"'):
                    line = replace_str(line, new_options)

                    # Makes sure the loop continues.
                    inside_game = False

            print(line, end='')
