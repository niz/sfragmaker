# Description: Manages configurations for sfragmaker.
# Requires: Python 3.11
# --------------------------------------------------------------

import tomllib
from copy import deepcopy
from os import environ
from pathlib import Path
from types import SimpleNamespace

"""Default configurations file name."""
config_file = 'sfragmaker.toml'

def load(force_default=False):
    """Loads configurations from files."""

    config = load_default()
    custom = load_custom()

    if custom:
        user_override(config, custom)

    resolve_paths(config)
    return to_namespace(config)

def user_override(default, custom):
    """Overrides default configurations with user defined ones."""

    for key in default.keys():
        if custom.get(key):
            if type(custom[key]) is dict:
                user_override(default[key], custom[key])
            else:
                default[key] = custom[key]

def resolve_paths(config):
    """Expands user and resolve every expected path."""

    # List of (section, option) pairs.
    configs = [
        ('game', 'game_dir'),
        ('game', 'demos_dir'),
        ('export', 'demos_dir'),
        ('export', 'frames_dir'),
        ('export', 'videos_dir'),
        ('steam', 'localconfig'),
    ]

    for pair in configs:
        if pair[0] not in config.keys() :
            raise Exception(f"Missing configuration section: {pair[0]}")

        path = config.get(pair[0]).get(pair[1])
        if not path:
            raise Exception(f"Missing configuration: {pair[1]}")

        # Expand allowed HOME var.
        path = path.format('{HOME}', Path.home())

        # Transform into pathlib.Path object.
        path = Path(path).expanduser().resolve()
        config[pair[0]][pair[1]] = path

def load_default():
    """Loads default configurations from file."""

    file = get_default_file()
    return read_file(file)

def load_custom():
    """Loads user defined configurations from file."""

    file = get_custom_file()
    if not file:
        return None

    return read_file(file)

def read_file(file_path):
    """Reads and parses info from configurations file."""

    with open(file_path, 'rb') as f:
        return tomllib.load(f)

def get_default_file():
    """Resolves default configurations file path."""

    # Default configurations directory: ../../resources/config/
    main_dir = Path(__file__).resolve().parents[1]
    config_dir = main_dir.joinpath('resources', 'config')

    if not config_dir:
        raise Exception('Missing default configuration directory.')

    file = Path(config_dir, config_file)
    if not file.is_file():
        raise Exception('Missing default configurations file.')

    return file.resolve()

def get_custom_file():
    """Resolves user defined configurations file path."""

    config_home = get_config_home()
    if not config_home:
        return None

    file = Path(config_home, config_file)
    if not file.is_file():
        return None

    return file.resolve()

# TODO: Make this function platform independent.
def get_config_home():
    """Resolves default user configurations directory path."""

    try:
        config_home = "{XDG_CONFIG_HOME}".format(**environ)
        return Path(config_home).resolve()

    # $XDG_CONFIG_HOME is not defined.
    except KeyError:
        config_home = None

    if not config_home:
        config_home = Path.home().joinpath('.config')
        config_dir = Path(config_home, 'sfragmaker')

        if config_dir.exists():
            return config_dir.resolve()
        else:
            return None

def to_namespace(var):
    """Helper function to turn dictionary into namespace."""

    if type(var) is dict:
        for key, value in var.items():
            if type(value) is dict:
                var[key] = to_namespace(value)
        return SimpleNamespace(**var)
    else:
        return var
