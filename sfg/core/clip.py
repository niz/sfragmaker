# Description: Manages clips.
# Requires: Python 3.11
# Repository: https://codeberg.org/niz/sfragmaker
# --------------------------------------------------------------

import shutil
import sys
from datetime import datetime
from pathlib import Path
from uuid import uuid4

from . import cfg
from . import config
from . import ticktime as ttime

class Clip:
    """Manages clips for sfragmaker."""

    def __init__( self, **kwargs):
        if not kwargs.get('name'):
            raise Exception('Name argument is required.')

        self.set_name(kwargs.get('name'))
        self.set_uuid(kwargs.get('uuid'))
        self.description = kwargs.get('description')

        self.set_bounds(
            kwargs.get('start'),
            kwargs.get('end'),
            kwargs.get('length'))

        if kwargs.get('tags'):
            self.tags = kwargs.get('tags')
        else:
            self.tags = []

# --------------------------------------------------------------
# Static Methods
# --------------------------------------------------------------

    @staticmethod
    def get_current_uuid():
        """Return the uuid of currently loaded clip."""
        try:
            return cfg.read('setclip').get('uuid')
        except FileNotFoundError:
            return None

    @staticmethod
    def export_clip(name, destination=None):
        """Exports audio and frame files from game directory."""

        cf = config.load()
        game_dir = cf.game.game_dir

        # Check for frame files in directory.
        if not next(game_dir.glob(f"{name}*.tga"), None):
            print(f"Could not find any frame files for clip {name}.", file=sys.stderr)
            exit()

        if not destination:
            # Append time stamp to output directory name to avoid
            # mixing up with existing clip files.
            dir_name=f"{name}_{datetime.now().isoformat()}"
            destination = cf.export.frames_dir.joinpath(dir_name)

        # Create directory.
        destination = Path(destination).resolve()
        destination.mkdir(parents=True)

        for frame in game_dir.glob(f"{name}*.tga"):
            print(f"Moving frame: {frame.name}", file=sys.stderr)
            shutil.move(frame, destination)

        for audio in game_dir.glob(f"{name}*.wav"):
            if not audio:
                print(f"Could not find any audio files for clip {audio}")

            print(f"Moving audio: {audio.name}", file=sys.stderr)
            shutil.move(audio, destination)

        print(f"{destination}")

    @staticmethod
    def remove_cfg():
        """Remove cfg files from game directory."""
        Clip.remove_cfg_setclip()
        Clip.remove_cfg_record()

    @staticmethod
    def remove_cfg_setclip():
        """Remove cfg file from game directory."""
        cfg.remove('setclip')

    @staticmethod
    def remove_cfg_record():
        """Remove cfg file from game directory."""
        cfg.remove('record')

# --------------------------------------------------------------
# Public Methods
# --------------------------------------------------------------

    def load(self):
        """Create cfg files to automate clip setting and recording."""
        self.write_cfg_setclip()
        self.write_cfg_record()

    def unload(self):
        """Remove cfg files from game directory."""
        Clip.remove_cfg()

    def update(self, **kwargs):
        """Update clip's attributes."""

        name = kwargs.get('name')
        if name:
            self.set_name(name)

        description = kwargs.get('description')
        if description:
            self.description = description

        tags = kwargs.get('tags')
        if tags:
            self.tags = args

        args = {
            'start': kwargs.get('start'),
            'end': kwargs.get('end'),
            'length': kwargs.get('length')
        }

        if any(args.values()):
            """Rules for updating bounds are slightly different
            from those for creating clips."""

            if args['start'] and not args['end'] and not args['length']:
                args['end'] = self.end_time

            if args['end'] and not args['start'] and not args['length']:
                args['start'] = self.start_time

            if args['length'] and not args['start'] and not args['end']:
                args['start'] = self.start_time

            if not args['start'] and not args['end'] and not args['length']:
                args['start'] = self.start_time
                args['end'] = self.end_time

            self.set_bounds(**args)

    def report(self, show_all=False, prepend=None, append=None):
        """Create a text report to be printed."""

        # Line to be used as separator
        separator = "{SEPARATOR}"

        output = prepend + separator if prepend else ''

        output += f"{self.name}"
        output += separator

        output +=(f"Start: {self.start_time}\n"
                  f"End: {self.end_time}")
        output += separator
        output += f"Duration: {self.length_time}"
        output += separator

        if show_all:
            output +=(f"Start Tick: {self.start_tick}\n"
                      f"End Tick: {self.end_tick}\n"
                      f"Length: {self.total_ticks}")
            output += separator

            cf = config.load()

            tps = cf.game.tps
            secs = ttime.ticks_to_secs(self.total_ticks, tps=tps)
            frames = cf.encoding.frame_rate*secs

            output += f"Frames: {frames}"
            output += separator

            if self.description:
                output += f"Description: {self.description}"
                output += separator

        output += append if append else ''
        return output

    def export(self, destination=None):
        """Export clip files to destination directory."""
        self.export_clip(self.name, destination=destination)

    def to_dict(self):
        """Return a dictionary with clip's atributes."""
        # keep this order in file
        info =  {
            'uuid': self.uuid,
            'name': self.name,
            'start': self.start_time,
            'end': self.end_time,
            'description': self.description,
        }

        if self.tags:
            info['tags'] = self.tags

        return info

# --------------------------------------------------------------
# Internal Methods
# --------------------------------------------------------------

    def set_name(self, name):
        """Set clip's name"""
        self.name = name.replace(' ','_').lower()

    def set_uuid(self, uuid=None):
        """Set clip's uuid."""
        self.uuid = str(uuid4()) if not uuid else uuid

    def set_bounds(self, start=None, end=None, length=None):
        """Set clip's start and end bounds."""

        interval = self.tick_interval(start, end, length)

        self.start_tick = interval[0]
        self.end_tick = interval[1]
        self.total_ticks = self.end_tick - self.start_tick

        self.start_time = ttime.ticks_to_time(self.start_tick)
        self.end_time = ttime.ticks_to_time(self.end_tick)
        self.length_time = ttime.ticks_to_time(self.total_ticks)

    def tick_interval(self, start=None, end=None, length=None):
        """Define clip's interval in ticks."""

        if start:
            if end:
                # start and end: play from start to end.
                start_tick = ttime.time_to_ticks(start)
                end_tick = ttime.time_to_ticks(end)

                if start_tick > end_tick:
                    raise Exception(
                        f"Start time {start} bigger than end time {end}.")

                return (start_tick, end_tick)

            elif length:
                # start and length: play from start to length.
                start_tick = ttime.time_to_ticks(start)
                return (start_tick, start_tick + ttime.time_to_ticks(length))

            else:
                # only start: play from start until end of demo.
                return (ttime.time_to_ticks(start), 0)

        elif end:
            if length:
                # end and length: play from (end - length) to end.
                end_tick = ttime.time_to_ticks(end)
                start_tick = end_tick - ttime.time_to_ticks(length)

                if start_tick < 0:
                    start_tick = 0

                return (start_tick, end_tick)

            else:
                # only end: play from start of demo to end.
                return (0, ttime.time_to_ticks(end))

        else:
            if length:
                # only length: play from start of demo to length.
                return (0, ttime.time_to_ticks(length))

            else:
                # play entire demo.
                return (0,0)

    def write_cfg_setclip(self):
        """Create a file with commands to set a new clip."""

        data = {
            'uuid': self.uuid,
            'name': self.name,
            'end_tick': self.end_tick,
            'start_tick': self.start_tick,
        }

        cfg.write('setclip', data)

    def write_cfg_record(self):
        """Create a file with commands to record a new clip."""

        encoding = config.load().encoding
        data = {
            'uuid': self.uuid,
            'name': self.name,
            'frame_rate': encoding.frame_rate
        }

        cfg.write('record', data)
