# Description: Manages demo files.
# Requires: Python 3.11
# --------------------------------------------------------------

import json
import os
import shutil
import subprocess
import sys

from datetime import datetime, timedelta
from hashlib import sha256
from pathlib import Path

from . import cfg
from . import config
from . import decoder
from .clip import Clip

class Demo:
    """Manages demo files."""

    def __init__(self, file_path, read_json=True):
        """Create a new Demo object.

        Parameters:
            file_path: Path to demo file (.dem).
            load_json: Boolean. Attempt to load info file (.json).
        """
        self.file = self.resolve_file_path(file_path)
        self.info_file = self.file.with_suffix(".json")

        self.name = self.file.stem
        self.description = ''

        self.decoded_info = self.decode()
        self.client = self.decoded_info['client']
        self.frames = self.decoded_info['frames']
        self.server = self.decoded_info['server']
        self.map = self.decoded_info['map']
        self.ticks = self.decoded_info['ticks']

        self.seconds = self.decoded_info['playback_time']
        self.playback_time = timedelta(seconds=round(self.seconds))

        self.clips = []
        self.events = []
        self.tags = []

        # Modified time (last saved by the game).
        self.mtime = datetime.fromtimestamp(
            os.path.getmtime(self.file))

        self.hash = []

        self.has_info = False
        if read_json:
            self.load_info()

# --------------------------------------------------------------
# Static and Class Methods
# --------------------------------------------------------------

    @staticmethod
    def listdir(directory=None):
        """List demo files inside directory.

        Parameters:
            directory: Path to directory in which to look for demo files.
        Returns:
            iterator: List of demo files.
        """
        if not directory:
            # Use default demo directory.
            game_dir = config.load().game.game_dir
            directory = Path(game_dir, 'demos')
        else:
            directory = Path(directory).resolve()

        if not directory.is_dir():
            raise Exception(f"Not a directory: {directory}")

        return directory.glob('*.dem')

    @classmethod
    def get_current(cls):
        """Get currently loaded demo.

        Get demo currently set by playdemo cfg file.

        Returns:
            Demo object: Current demo.
        """
        try:
            info = cfg.read('playdemo')
            if not info:
                return None

            return cls(info['demo'])

        except FileNotFoundError:
            return None

# --------------------------------------------------------------
# Public Methods
# --------------------------------------------------------------

    def load(self):
        """Load demo file.

        Creates cfg file to automate demo playback.
        """
        cfg.write('playdemo', {
            'hash': self.get_hash()['digest'],
            'demo': self.file.absolute(),
        })

    def unload(self):
        """Unload current demo.

        Unloads current demo by removing file from game directory.
        """
        cfg.remove("playdemo")

    def destroy(self, confirm=True):
        """Delete demo file."""
        print('TODO: Delete demo file')

    def export(self, destination=None):
        """Export demo from game directory.

        Improve demo info and move files to destination.

        Parameters:
            destination: Path to directory in which to move files.
        """
        self.save_info()

        # TODO: Make default exported name configurable.
        mtimestr = self.mtime.strftime('%Y-%m-%d_%H-%M-%S')
        new_name = f"{self.map}_{self.client}_{mtimestr}"

        if not destination:
            destination = config.load().export.demos_dir

        dst = Path(destination).resolve()
        if not dst.is_dir:
            raise Exception(f"Not a directory: {dst}")

        new_path = Path(dst, f"{new_name}.dem")
        self.move(new_path)

    def move(self, new_path):
        """Move demo file.

        Move demo (.dem) and info (.json) files to destination.

        Parameters:
            new_path: Path to new destination.
        """
        new_path = Path(new_path)
        if new_path.exists():
            raise Exception(
                f"File or directory already exists: {new_path.resolve()}")

        self.file = shutil.move(self.file, new_path)

        if self.info_file.is_file():
            new_info_path = new_path.with_suffix(".json")
            self.info_file = shutil.move(self.info_file, new_info_path)

    def report(self, minimal=False, complete=False, prepend='', append=''):
        """Report demo info in text format."""
        output = prepend

        if minimal:
            output += self.make_minimal_report()
        else:
            output += self.make_report()
            if complete:
                output += self.make_clips_report()
                output += self.make_events_report()
                output += self.make_tags_report()

        return output + append

    def report_clip(self, uuid=None, index=None, name=None):
        """Report clip info in text format."""
        clip = self.get_clip(uuid=uuid, index=index, name=name)
        if not clip:
            return 'Could not find clip.'

        return clip.report(
            prepend=f" Clip from {self.file}",
            show_all=True)

    def report_list(self, option, prepend='', append=''):
        """Reports a list of items."""
        makers = {
            'clips': self.make_clips_report,
            'events': self.make_events_report,
            'tags': self.make_tags_report,
        }

        output = prepend
        output += f"{self.file}"
        output += '{SEPARATOR}'
        report = makers[option]()
        output += report if report else f"No {option} were fond."

        return output + append

    def add_clip(self, clip):
        """Add new clip to demo."""
        if type(clip) is dict:
            clip = Clip(**clip)

        self.clips.append(clip)
        self.save_info()

    def add_event(self, event):
        """Add event to demo."""
        if not type(event) is dict:
            raise TypeError

        if not self.has_info:
            self.load_info()

        self.events.append(event)
        self.events.sort(key=lambda e: int(e['tick']))
        self.save_info()

    def add_tags(self, tag_string):
        """Add tags to demo."""
        if not self.has_info:
            self.load_info()

        new_tags = filter(None,[tag.strip().lower() for tag in tag_string.split(' ')])
        for tag in new_tags:
            if tag not in self.tags:
                self.tags.append(tag)

        self.tags.sort()
        self.save_info()

    def remove_clip(self, index=None, uuid=None):
        """Removes a clip from the list."""
        if not self.has_info:
            self.load_info()

        if not uuid:
            if index:
                clip = self.get_clip(index=index)
            else:
                return
        else:
            clip = self.get_clip(uuid=uuid)

        if not clip:
            return

        self.clips.remove(clip)
        self.save_info()

    def remove_event(self, index):
        """Remove event from demo."""
        if not self.has_info:
            self.load_info()

        # Index is offset by 1.
        index -= 1
        if index < 0 or index >= len(self.events):
            return

        self.events.pop(index)
        self.save_info()

    def remove_tag(self, index):
        """Remove tags from demo."""
        if not self.has_info:
            self.load_info()

        # Index is offset by 1.
        index -= 1
        if index < 0 or index >= len(self.tags):
            return

        self.tags.pop(index)
        self.save_info()

    def get_clip(self, uuid=None, index=None, name=None):
        """Get a clip from clips list."""
        if not self.clips:
            return None

        if uuid:
            for clip in self.clips:
                if clip.uuid == uuid:
                    return clip
        elif index:
            # Index is offset by 1.
            index -= 1
            if index < 0 or index >= len(self.clips):
                return None

            return self.clips[index]
        elif name:
            for clip in self.clips:
                if clip.name == name:
                    return clip

        return None

    def get_event(self, index):
        """Get event from demo."""
        if not self.events:
            return None

        # Index is offset by 1.
        index -= 1
        if index < 0 or index >= len(self.events):
            return None

        return self.events[index]

    def update_clip(self, **clip_info):
        """Update clip with new data."""
        if 'uuid' not in clip_info:
            raise Exception('uuid is required.')

        clip = self.get_clip(clip_info['uuid'])
        clip.update(**clip_info)
        self.save_info()

        return clip

    def save_info(self):
        """Save info to file."""
        self.write_info_file()
        self.load_info()

# --------------------------------------------------------------
# Internal Methods
# --------------------------------------------------------------

    def decode(self):
        """Get demo info from binary file."""
        try:
            with open(self.file, 'rb') as file:
                return decoder.decode(file)
        except:
            pass

        raise Exception(f"Could not decode file: {self.file}")

    def make_report(self):
        """Make text report of demo information."""
        separator = '{SEPARATOR}'

        output = f"{self.file}"
        output += separator

        output += (f"Client: {self.client} "
                   f"| Map: {self.map} "
                   f"| Server: {self.server}")

        output += separator

        output += f"Playback Time: {self.playback_time}"
        output += separator

        output += (f"Frames: {self.frames} "
                   f"| Ticks: {self.ticks}")
        output += separator

        output += (f"Clips: {len(self.clips)} "
                   f"| Events: {len(self.events)} "
                   f"| Tags: {len(self.tags)}")
        output += separator

        if self.description:
            output += f"Description: {self.description}"
            output += separator

        return output

    def make_clips_report(self):
        if not self.clips:
            return ''

        output = 'Clips: \n\n'
        for index, clip in enumerate(self.clips):
            output += (f" [{index + 1}] {clip.name}"
                       f" ({clip.start_time} - {clip.end_time})\n")

            if clip.description:
                output += f" Description: {clip.description}\n"

            if clip.tags:
                output += f" {'-'*8}\n"
                output += f" Tags: {', '.join(clip.tags)}\n"
            output += '\n'

        output += '{SEPARATOR}'
        return output

    def make_events_report(self):
        """Make text report for events."""
        if not self.events:
            return ''

        output = "Events:\n\n"
        for index, event in enumerate(self.events):
            output += (f" [{index + 1}] {event.get('tick')}"
                       f" {event.get('name')}"
                       f" {event.get('value')}\n")

        output += '{SEPARATOR}'
        return output

    def make_tags_report(self):
        """Make text report for tags."""
        if not self.tags:
            return ''

        output = "Tags:\n\n"
        for index, tag in enumerate(self.tags):
            output += (f" [{index + 1}] {tag}\n")

        output += '{SEPARATOR}'
        return output

    def make_minimal_report(self):
        """Make report with minimal information only."""
        return (f"{self.client} {self.map} {self.server} "
                f"{self.playback_time} {self.seconds} "
                f"{self.ticks} {self.frames}")

    def load_info(self):
        """Load info from file."""
        info = self.read_info_file()

        self.description = info.get('description')

        events = info.get('events')
        self.events = events if events else self.events

        tags = info.get('tags')
        self.tags = tags if tags else self.tags

        if 'clips' in info:
            self.load_clips(info['clips'])

        self.has_info = True

    def load_clips(self, info_clips):
        """Read clips from info."""
        if type(info_clips) is not list:
            raise TypeError

        # Create list of Clip objects.
        clips = [Clip(**clip) for clip in info_clips]

        if self.clips:
            for clip in clips:
                # Check for new clips.
                if not self.get_clip(uuid=clip.uuid):
                    self.clips.append(clip)
        else:
            self.clips = clips

        # Sort by start tick.
        self.clips.sort(key=lambda x: x.start_tick)

    def read_info_file(self, create_file=True):
        """Read info file if found."""

        if not self.info_file.is_file():
            if not create_file:
                print(('WARNING: Could not find info file'
                       f" {self.info_file}"),
                      file=sys.stderr)
                return {}
            else:
               self.write_info_file()
               return self.read_info_file()

        with open(self.info_file, 'r') as f:
            info = json.load(f)
            if not self.check_hash(info):
                raise Exception(
                    (f"Hash mismatch: \"{self.path}\""
                     f" and \"{self.info_file}\""))

            return info

    def write_info_file(self):
        """Write info to file."""
        file_info = self.read_info_file(create_file=False)

        file_info['hash'] = self.get_hash()

        file_info['description'] = self.description
        file_info['events'] = self.events
        file_info['tags'] = self.tags

        if self.clips:
            file_info['clips'] = []
            # Get dictionaries from Clip objects
            for clip in self.clips:
                file_info['clips'].append(clip.to_dict())

        with open(self.info_file, 'w') as f:
            f.write(json.dumps(file_info, indent=4))

    def resolve_file_path(self, file_path):
        """Resolve path to demo file."""
        if isinstance(file_path, str):
            file_path = Path(file_path.strip())

        if not file_path.is_file():
            # Check if file_path was given without file extension.
            file_path = file_path.with_suffix(".dem")
            if not file_path.is_file():
                raise FileNotFoundError(
                    f"Could not locate {file_path.resolve()}")

        return file_path.expanduser().resolve()

    def check_hash(self, info):
        """Compares file hash with hash from info."""
        if 'hash' in info:
            return info['hash'] == self.get_hash()

        else:
            print(
                'WARNING: Info file does not contain hash field!',
                file=sys.stderr)
            return True

    def get_hash(self):
        """Gets file hash."""
        if not self.hash:
            self.hash = self.hash_contents()

        return self.hash

    def hash_contents(self):
        """Hashes file's contents using SHA-256."""

        # Hash in 64kb chunks.
        BUF_SIZE = 65536

        hash_obj = sha256()
        with open(self.file, 'rb') as f:
            while True:
                content = f.read(BUF_SIZE)
                if not content:
                    break
                hash_obj.update(content)

        return {
            'algorithm': 'SHA-256',
            'digest': hash_obj.hexdigest()
        }

