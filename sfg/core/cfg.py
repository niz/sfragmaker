# Description: Manages cfg files created by sfragmaker.
# Requires: Python 3.11
# --------------------------------------------------------------

import io
import os
import tomllib
from pathlib import Path

from . import config

"""Files created and used by sfragmaker."""
files = ['playdemo', 'record', 'setclip']

"""Lines containing data on each file."""
data_lines = {'setclip': 2, 'playdemo': 2, 'record': 2}

def read(file_name):
    """Read cfg file and return its data.

    Attempt to locate cfg file by name and read its
    toml encoded data.

    Parameters:
        file_name (string): Name of file to be read.
    Returns:
        dictionary: File data.
    """
    file_path = get_file_path(file_name)
    if not file_path.is_file():
        raise FileNotFoundError(
            f"Could not locate {file_path.resolve()}")

    with open(file_path, 'r') as file:
        return read_file(file, data_lines[file_name], skip=5)

def remove(file_name):
    """Remove cfg file."""
    if file_name not in files:
        raise FileNotFoundError(
            f"Unexpected cfg file name: {file_name}")

    file_path = get_file_path(file_name)
    if file_path.is_file():
        os.remove(file_path)

def write(file_name, data):
    """Write cfg file.

    Encode data and write cfg file into game's cfg directory.

    Parameters:
        file_name (string): Name of file to be written.
        data (dictionary): Data to be written to file.
    """
    file_path = get_file_path(file_name)
    res_path = get_resource_file_path(file_name)

    with open(res_path, 'r') as res, open(file_path, 'w') as out:
        out.write(format_file(res, data))

def format_file(resource_file, data):
    """Format text to be written to new cfg file.

    Format text and insert data into text to be written to cfg file.

    Parameters:
        resource_file (file object): Resource file with original text.
        data (dictionary): Data to be added to text.
    Returns:
        string: Formatted text.
    """
    return resource_file.read().format(**data)

def get_resource_file_path(file_name, force_default=False, resources_dir=None):
    """Get path to file from resources directory.

    Looks for resource file matching file name and
    returns its path.

    Parameters:
        file_name (string): Name of file to find.
        force_default (boolean): Load only default configurations.
        resources_dir (Path object): Path to resources directory.
    Returns:
        Path object: Path to resource file.
    """
    file = f"{file_name}.cfg"

    if not resources_dir:
        resources_dir = get_resources_dir(force_default=force_default)

    # Load default file.
    resource_path = resources_dir.joinpath(file)
    if not resource_path.is_file():
        raise FileNotFoundError(
            f"Missing default cfg file: {resource_path}.")

    return resource_path.resolve()

def get_resources_dir(force_default=False):
    """Get resources directory.

    Look for user defined or default resources directory
    and return its path.

    Parameters:
        force_default: Use default configurations only.
    Returns:
        Path object: Path to resources directory.
    """
    # Check for user defined resources directory.
    config_home = config.get_config_home()
    if config_home and not force_default:
        cfg_dir = config_home.joinpath('cfg').resolve()

        if cfg_dir.is_dir():
            return cfg_dir

    # Default cfg directory: ../../resources/config/cfg/
    main_dir = Path(__file__).resolve().parents[1]
    default_dir = main_dir.joinpath('resources', 'config', 'cfg')

    if not default_dir:
        raise Exception('Missing default cfg resources directory.')

    return default_dir.resolve()

def get_file_path(file_name, force_default=False):
    """Get path for cfg file inside game directory.

    Check if file name refers to a valid cfg file
    and return its path inside game's cfg directory.

    Parameters:
        file_name (string): Name of file.
    Returns:
        Path object: File path.
    """
    if file_name not in files:
        raise FileNotFoundError(
            f"Unexpected cfg file name: {file_name}")

    cf = config.load(force_default=force_default)
    cfg_prefix = cf.game.cfg_prefix
    game_dir = cf.game.game_dir

    return game_dir.joinpath(
        'cfg',
        f"{cfg_prefix}{file_name}.cfg")

def read_file(file, lines=1, skip=0):
    """Read cfg file and return its toml encoded data.

    Read cfg file created by sfragmaker and extract its
    header data in toml format.

    Parameters:
        file (file object): cfg file to be read.
        lines (integer): Number of lines to be read.
        skip (integer): Number of lines to be skipped before relevant data.
    Returns:
        dictionary: File's data.
    """
    toml_info = ''

    # Removing comments and extracting only relevant info.
    file_lines = file.readlines()[skip:lines+skip]
    for line in file_lines:
        toml_info += line[3:]

    toml_file = io.BytesIO(bytes(toml_info, 'UTF-8'))
    return tomllib.load(toml_file)
