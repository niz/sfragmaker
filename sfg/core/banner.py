# Description: Reads sfragmaker's banner from resource file.
# Requires: Python 3.11
# --------------------------------------------------------------

from pathlib import Path

def get():
    """Gets the banner text from resource file."""

    # Resource file should be at ../../resources/banner.txt
    main_dir = Path(__file__).resolve().parents[2]
    banner_file = main_dir.joinpath('resources', 'banner.txt')

    with open(banner_file, 'r') as f:
        return f.read()

def show(hide=False):
    """Prints banner if not explicitly hidden."""

    if not hide:
        print(get())
