# Description: Decodes basic information from demo files. More
# info at https://developer.valvesoftware.com/wiki/DEM_Format.
# --------------------------------------------------------------

import struct

def decode(file):
    """Read bytes from demo file and decode their info.

    Parameters:
        file (file object): Demo file to be decoded.
    Returns:
        dictionary: Decoded information.
    """
    header = str_decode(file.read(8))
    if not header == 'HL2DEMO':
        raise TypeError(f"Not a demo file.")

    try:
        return {
            'header': header,
            'demo_protocol': int_decode(file.read(4)),
            'network_protocol': int_decode(file.read(4)),
            'server': str_decode(file.read(260)),
            'client': str_decode(file.read(260)),
            'map': str_decode(file.read(260)),
            'game_directory': str_decode(file.read(260)),
            'playback_time': float_decode(file.read(4)),
            'ticks': int_decode(file.read(4)),
            'frames': int_decode(file.read(4)),
            'sign_on_length': int_decode(file.read(4)),
        }

    except:
        pass

    raise ValueError('Could not decode demo file.')

def str_decode(byte_str):
    """Decode bytes into string."""
    if not byte_str:
        raise ValueError('Empty byte string.')

    return byte_str.decode("UTF-8").replace('\x00', '')

def int_decode(byte_str):
    """Decode bytes into integer."""
    if not byte_str:
        raise ValueError('Empty byte string.')

    return int.from_bytes(byte_str, 'little')

def float_decode(byte_str):
    """Decode bytes into float."""
    if not byte_str:
        raise ValueError('Empty byte string.')

    return struct.unpack('f', byte_str)[0]
