#!/usr/bin/env python3.11
# Description: sfg commands package.
# Requires: Python 3.11
# --------------------------------------------------------------

__prog__ = 'sfg'
__version__ = '2023.04a1'

import argparse
from pathlib import Path

def valid_demo_file(file):
    """Function to help arparse to determine a valid demo file."""

    ext = Path(file).suffix
    if ext.lower() != '.dem':
        raise argparse.ArgumentTypeError('Demo file must have a .dem extension.')

    return file
