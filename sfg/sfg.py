#!/usr/bin/env python3.11
# Description: Top level command for sfragmaker.
# Requires: Python 3.11
# --------------------------------------------------------------

import argparse
from sys import argv

from . import __version__
from . import __prog__
from . import current
from . import export
from . import load
from . import probe
from . import render
from . import unload

def main():
    """Command's entry point."""

    # Command's name.
    prog = __prog__

    # Defined subcommands.
    subcommands = {
        'current': current.main,
        'export': export.main,
        'load': load.main,
        'probe': probe.main,
        'render': render.main,
        'unload': unload.main,
    }

    # Command: sfg
    # ---------------------
    parser = argparse.ArgumentParser(
        prog=prog,
        description='Source Fragmaker (sfragmaker).')

    parser.add_argument(
        '-v',
        '--version',
        action='version',
        version=f"{prog}'s version: {__version__}")

    parser.add_argument(
        'subcommand',
        type=str,
        choices=subcommands.keys(),
        help='Subcommand to be called.')

    # When called without arguments, print usage instead.
    if len(argv) < 2:
        parser.print_usage()
        exit()

    # Only consider first argument as valid.
    first = [argv[1]]
    sub_name = parser.parse_args(first).subcommand

    # Use anything else as subcommand argument.
    sub_args = argv[2:]

    # Call subcommand main function with remaining arguments.
    subcommands[sub_name](sub_args, f"{prog} {sub_name}")

if __name__ == '__main__':
    main()
