#!/usr/bin/env python3.11
# Description: Removes sfragmaker files from game directory.
# Requires: Python 3.11
# --------------------------------------------------------------

import argparse

from .core import cfg
from .core.demo import Demo

def clear():
    """Remove files from game directory."""
    current_demo = Demo.get_current()
    if current_demo:
        current_demo.unload()

    cfg.remove('setclip')
    cfg.remove('record')
    cfg.remove('playdemo')

def main(args=None, prog='unload'):
    """Main module's entry point."""

    # Root command: unload.
    # --------------------------------
    parser = argparse.ArgumentParser(
        prog=prog,
        description='Removes sfragmaker files from game directory.')

    # Parsing arguments.
    # --------------------------------
    args = parser.parse_args(args)
    clear()

if __name__ == '__main__':
    main()
