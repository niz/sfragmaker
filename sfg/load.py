#!/usr/bin/env python3.11
# Description: Manages loading of clips, demos and game settings
# profiles for sfragmaker.
# Requires: Python 3.11
# --------------------------------------------------------------

import argparse

from . import info
from . import reporter
from . import valid_demo_file
from .core import Demo
from .core import profile as Profile

def clip(args):
    """Load clip."""
    demo = info.get_demo_or_fail()
    demo.get_clip(index=args.index).load()

def demo(args):
    """Load demo files."""
    # Unloads previous demo.
    prev_demo = Demo.get_current()
    if prev_demo:
        prev_demo.unload()

    demo = Demo(args.file)
    demo.load()

def profile(args):
    """Loads game settings profile."""
    try:
        Profile.load(args.name)
        reporter.report(f"Loaded game settings profile: {args.name}")
    except Exception as e:
        print(e)
        exit()

def main(args=None, prog='load'):
    """Main module's entry point."""

    # Root command: load.
    # --------------------------------
    parser = argparse.ArgumentParser(
        prog=prog,
        description='Loads clips, demos and profiles for sfragmaker.')

    subparsers = parser.add_subparsers(help='subcommand help')

    # Subcommand: clip
    # ---------------------
    clip_parser = subparsers.add_parser(
        'clip',
        description='Load clip from currently loaded demo.',
        help='clip --help')

    clip_parser.add_argument(
        'index',
        type=int,
        help='Index of clip to be loaded.')

    clip_parser.set_defaults(func=clip)

    # Subcommand: demo
    # ---------------------
    demo_parser = subparsers.add_parser(
        'demo',
        description='Load demo.',
        help='demo --help')

    demo_parser.add_argument(
        'file',
        type=str,
        help='Demo file to be loaded.')

    demo_parser.set_defaults(func=demo)

    # Subcommand: profile
    # ---------------------
    profile_parser = subparsers.add_parser(
        'profile',
        description='Load settings profile.',
        help='demo --help')

    profile_parser.add_argument(
        'name',
        type=str,
        help='Game settings profile to be loaded.')

    profile_parser.set_defaults(func=profile)

    # Parsing arguments.
    # --------------------------------
    args = parser.parse_args(args)

    # When called without arguments.
    if not any(vars(args).values()):
        parser.print_usage()
        exit()

    # Execute sub command functions
    args.func(args)

if __name__ == '__main__':
    main()
