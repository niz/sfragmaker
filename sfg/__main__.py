#!/usr/bin/env python3.11
# Description: Main entry point for sfragmaker's sfg package.
# Requires: Python 3.11
# Repository: https://codeberg.org/niz/sfragmaker
# --------------------------------------------------------------

from . import sfg

def main():
    sfg.main()

if __name__ == '__main__':
    main()
