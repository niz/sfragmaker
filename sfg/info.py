#!/usr/bin/env python3.11
# Description: Displays information about currently loaded demo.
# Requires: Python 3.11
# --------------------------------------------------------------

import argparse

from . import reporter
from .core import Clip
from .core import Demo

def get_clip():
    demo = Demo.get_current()
    if not demo:
        return None

    uuid = Clip.get_current_uuid()
    if uuid:
        clip = demo.get_clip(uuid=uuid)
        if clip:
            return clip
        # If it didn't find the clip in info file,
        # last cfg file probably wasn't removed correctly.
        else:
            Clip.remove_cfg()
    return None

def get_demo_or_fail():
    """Get current demo object."""
    demo = Demo.get_current()
    if not demo:
        reporter.exit_with('missing_current_demo')

    return demo

def get_clip_or_fail():
    demo = get_demo_or_fail()
    clip = get_clip()
    if clip:
        return clip

    reporter.exit_with('missing_current_clip')

def report(args):
    """Report information about currently loaded demo and clip."""
    reporter.report(get_demo_or_fail().report(prepend='[Current Demo] ', complete=args.all))
    reporter.report(get_clip_or_fail().report(prepend='[Current Clip] '))

def main(args=None, prog='current'):
    """Main entry point for this module."""

    # Command: current.
    # --------------------------------
    parser = argparse.ArgumentParser(
        prog=prog,
        description=('Displays information about currently loaded'
                     ' demo and clip.'))

    parser.add_argument(
        '-a',
        '--all',
        action='store_true',
        help='Fetch full demo report.')

    # Parse arguments
    # --------------------------------
    args = parser.parse_args(args)
    report(args)

if __name__ == '__main__':
        main()

