#!/usr/bin/env python3.11
# Description: Probes demo info for sfragmaker.
# Requires: Python 3.11
# --------------------------------------------------------------

import argparse
from sys import argv

from . import current
from . import reporter
from . import valid_demo_file
from .core import Demo

def report(args):
    """Report demo info."""
    demo = Demo(args.demo)
    out = demo.report()

    out += demo.make_clips_report() if args.clips or args.all else ''
    out += demo.make_events_report() if args.events or args.all else ''
    out += demo.make_tags_report() if args.tags or args.all else ''

    reporter.report(out)

def main(args=None, prog='probe'):
    """Main entry point for this module."""

    # Root command: probe
    # ---------------------
    parser = argparse.ArgumentParser(
        prog=prog,
        description='Probes demo info for sfgragmaker.')

    parser.add_argument(
        'demo',
        type=valid_demo_file,
        help='Demo file to probe.')

    parser.add_argument(
        '--clips',
        '-c',
        action='store_true',
        help="List demo's clips.")

    parser.add_argument(
        '--events',
        '-e',
        action='store_true',
        help="Lists demo's events.")

    parser.add_argument(
        '--tags',
        '-t',
        action='store_true',
        help="Lists demo's tags.")

    parser.add_argument(
        '--all',
        '-a',
        action='store_true',
        help="Include all lists.")

    # Parsing all arguments.
    args = parser.parse_args(args)

    # When called without arguments.
    if not any(vars(args)):
        parser.print_usage()
        exit()

    report(args)
