#!/usr/bin/env python3.11
# Description: Edits clips and events from current demo.
# Requires: Python 3.11
# --------------------------------------------------------------

import argparse
import subprocess

from . import info
from . import list_
from . import reporter

def edit_clip(args):
    """Edit clip from current demo."""
    demo = info.get_demo_or_fail()
    clip = demo.get_clip(index=args.index)
    if not clip:
        reporter.exit_with(
            'missing_index', item='clip', index=args.index)

    new = {"uuid": clip.uuid}
    defaults = {
        "name": clip.name,
        "description": clip.description,
        "start": clip.start_time,
        "end": clip.end_time,
        "length": clip.length_time
    }

    reporter.report(f"[Current Demo]: {demo.file}"+'{SEPARATOR}')
    reporter.report(f" Updating clip:")

    for key, value in defaults.items():
        ipt = input(f" {key.capitalize()} ({value}): ")
        new[key] = ipt if ipt else value

    demo.update_clip(**new)

    print('\n')
    list_.main(args=['clips'])

def edit_event(args):
    """Edit event from current demo."""
    demo = info.get_demo_or_fail()
    event = demo.get_event(args.index)
    if not event:
        reporter.exit_with(
            'missing_index', item='event', index=args.index)

    new = {}

    reporter.report(f"[Current Demo]: {demo.file}"+'{SEPARATOR}')
    reporter.report(f" Updating event:")

    ip_tick = input(f" Tick ({event['tick']}): ")
    ip_name = input(f" Name ({event['name']}): ")
    ip_value = input(f" Value ({event['value']}): ")

    new['tick'] = ip_tick if ip_tick else event['tick']
    new['name'] = ip_name if ip_name else event['name']
    new['value'] = ip_value if ip_value else event['value']

    demo.remove_event(args.index)
    demo.add_event(new)

    print('\n')
    list_.main(args=['events'])

def edit_file(args):
    """Open demo info file (.json) in text editor."""
    demo = info.get_demo_or_fail()

    # TODO: Get preferred text editor from configs.
    subprocess.run(['vim', demo.info_file])




def main(args=None, prog='edit'):
    """Main module's entry point."""
    options = {
        'clip': edit_clip,
        'event': edit_event,
        'file': edit_file,
    }

    # Root command: edit.
    # --------------------------------
    parser = argparse.ArgumentParser(
        prog=prog,
        description='Edits clips and events from current demo file.')

    parser.add_argument(
        'item',
        type=str,
        choices=options.keys(),
        help='Type of item to be edited.')

    # Hacky way not to require index when using the 'file' option.
    if 'file' not in args:
        parser.add_argument(
            'index',
            type=int,
            help='Index of item to be edited.')

    # Parsing arguments.
    # --------------------------------
    args = parser.parse_args(args)

    # When called without arguments.
    if not any(vars(args).values()):
        parser.print_usage()
        exit()

    # Execute sub command functions
    options[args.item](args)

if __name__ == '__main__':
    main()
