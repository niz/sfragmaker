#!/usr/bin/env python3.11
# Description: Manages exports of movies and demos for sfragmaker.
# Requires: Python 3.11
# --------------------------------------------------------------

import argparse

from . import info
from . import reporter
from .core import Clip
from .core import config
from .core import Demo

def export_demos(args):
    """Export demos from game directory."""
    src = args.source
    if not src:
        src = config.load().game.demos_dir

    count = 0
    for file in Demo.listdir(src):
        reporter.report(f"Exporting {file} ...")
        demo = Demo(file)
        demo.export(args.destination)
        count += 1

    reporter.report(f"Total exported files: {count}")

def export_recording(args):
    """Export clip recording files from game directory."""
    if args.name:
        Clip.export_clip(args.name, args.destination)
        exit()
    else:
        clip = info.get_clip_or_fail()
        clip.export(destination=args.destination)

def main(args=None, prog='export'):
    """Command entry point."""

    # Root command: export.
    # --------------------------------
    parser = argparse.ArgumentParser(
        prog=prog,
        description='Exports demo files and clip recordings for sfragmaker.')

    subparsers = parser.add_subparsers(help='subcommand help')

    # Subcommand: movie.
    # --------------------------------
    rec_parser = subparsers.add_parser(
        'recording',
        description='Exports clip recording files.',
        help='recording --help')

    rec_parser.add_argument(
        '-n',
        '--name',
        type=str,
        help='Name of the movie to export. Defaults to current clip.')

    rec_parser.add_argument(
        '-d',
        '--destination',
        type=str,
        help='Destination directory to export files. Defaults to config.')

    rec_parser.set_defaults(func=export_recording)

    # Subcommand: demos.
    # --------------------------------
    demos_parser = subparsers.add_parser(
        'demos',
        description=('Moves all demo (.dem) and info (.json) files'
                     ' from game directory into specified one.'),
        help='demos --help')

    demos_parser.add_argument(
        '-d',
        '--destination',
        type=str,
        help=('Directory to which to export demo files.'
             ' Defaults to configuration.'))

    demos_parser.add_argument(
        '-s',
        '--source',
        type=str,
        help=('Directory from which to export demo files.'
              ' Defaults to configuration.'))

    demos_parser.set_defaults(func=export_demos)

    args = parser.parse_args(args)

    # When called without sub commands.
    if not any(vars(args).values()):
        parser.print_help()
        exit()

    # Execute sub command functions
    args.func(args)
