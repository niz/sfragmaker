#!/usr/bin/env python3.11
# Description: Adds clips, events or tags to current demo.
# Requires: Python 3.11
# --------------------------------------------------------------

import argparse

from . import info
from . import list_
from . import reporter

def new_clip(args):
    """Creates new clip."""
    demo = info.get_demo_or_fail()

    attr = {
        'description': None,
        'start': None,
        'end': None,
        'length': None,
        'tags': None,
    }

    reporter.report(f"[Current Demo]: {demo.file}"+'{SEPARATOR}')
    reporter.report(f" New clip:\n\n Name: {args.name}")

    for key in attr.keys():
        attr[key] = input(f" {key.capitalize()}: ")

    attr['name'] = args.name
    attr['tags'] = [t.strip().lower() for t in attr['tags'].split(' ')]

    demo.add_clip(attr)

    print('\n')
    list_.main(args=['clips'])

def new_event(args):
    """Add new event to demo."""
    demo = info.get_demo_or_fail()

    name = 'Bookmark'
    value = 'General'

    if not args.defaults:
        reporter.report(f"[Current Demo]: {demo.file}"+'{SEPARATOR}')
        reporter.report(f" New event:\n\n Tick: {args.tick}")
        ip_name = input(f" Name (default Bookmark): ")
        ip_value = input(f" Value (default General): ")

        name = ip_name if ip_name else name
        value = ip_value if ip_value else value

    demo.add_event({
        'name': name,
        'value': value,
        'tick': args.tick})

    print('\n')
    list_.main(args=['events'])

def new_tags(args):
    """Add new tags to demo."""
    demo = info.get_demo_or_fail()

    # Single tag.
    if hasattr(args, 'name'):
        demo.add_tags(args.name)
    else:
        reporter.report(f"[Current Demo]: {demo.file}"+'{SEPARATOR}')
        tags = input(f" New tags (separated by space): ")
        demo.add_tags(tags)

    print('\n')
    list_.main(args=['tags'])

def main(args=None, prog='add'):
    """Main module's entry point."""

    # Root command: add.
    # --------------------------------
    parser = argparse.ArgumentParser(
        prog=prog,
        description='Adds clips, events or tags to current demo file.')

    subparsers = parser.add_subparsers(help='subcommand help')

    # Subcommand: clip.
    # --------------------------------
    clip_parser = subparsers.add_parser(
        'clip',
        description='Adds clip to current demo.',
        help='clip --help')

    clip_parser.add_argument(
        'name',
        type=str,
        help="Clip name.")

    clip_parser.set_defaults(func=new_clip)

    # Subcommand: event.
    # --------------------------------
    event_parser = subparsers.add_parser(
        'event',
        description='Adds event to current demo.',
        help='event --help')

    event_parser.add_argument(
        'tick',
        type=int,
        help="Event's tick mark.")

    event_parser.add_argument(
        '--defaults',
        '-d',
        action='store_true',
        help="Use default values.")

    event_parser.set_defaults(func=new_event)

    # Subcommand: tag.
    # --------------------------------
    tag_parser = subparsers.add_parser(
        'tag',
        description='Adds single tag to current demo.',
        help='tag --help')

    tag_parser.add_argument(
        'name',
        type=str,
        help="Tag name.")

    tag_parser.set_defaults(func=new_tags)

    # Subcommand: tags.
    # --------------------------------
    tags_parser = subparsers.add_parser(
        'tags',
        description='Adds tags to current demo.',
        help='tags --help')

    tags_parser.set_defaults(func=new_tags)

    # Parsing arguments.
    # --------------------------------
    args = parser.parse_args(args)

    # When called without arguments.
    if not any(vars(args).values()):
        parser.print_usage()
        exit()

    # Execute sub command functions
    args.func(args)

if __name__ == '__main__':
    main()
